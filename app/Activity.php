<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    //
    protected $fillable = ['label', 'slug', 'username', 'actions'];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
