<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cours extends Model
{
    //
    protected $fillable = ['label', 'slug','categorie', 'description', 'resume', 'audio_url', 'video_url', 'professeur','duree'];

    public function matiere(){
        return $this->belongsTo(Matiere::class);
    }
}
