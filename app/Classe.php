<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classe extends Model
{
    //
    protected $fillable = ['label', 'slug','is_moment'];

    public function niveau(){
        return $this->belongsTo(Niveau::class);
    }

    public function matieres(){
        return $this->hasMany(Matiere::class);
    }
}
