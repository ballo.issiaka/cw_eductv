<?php

namespace App\Http\Controllers\front;

use App\Classe;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ClasseController extends Controller
{
    //
    public function index($id){
        $classe= Classe::find($id);
        return view('front/matieres_grid',compact('classe'));
    }
}
