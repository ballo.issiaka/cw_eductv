<?php

namespace App\Http\Controllers\front;

use App\Cours;
use App\Http\Controllers\Controller;
use App\Matiere;
use Illuminate\Http\Request;

class MatiereController extends Controller
{
    //
    public function index($matiere,$id){
        $matiere =  Matiere::find($matiere);
        if($id != "null")
            $cours = Cours::find($id);
        else
            $cours = $matiere->cours[0];
        $durations = 0;
        foreach($matiere->cours as $one){
            $durations+= $one->duree;
        }
        return view('front/cours_detail', compact('matiere', 'cours','durations'));
    }
}
