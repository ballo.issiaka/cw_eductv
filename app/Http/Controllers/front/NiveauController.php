<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use App\Niveau;
use Illuminate\Http\Request;

class NiveauController extends Controller
{
    //
    public function index($id){
        $niveau = Niveau::find($id);
        return view('front/niveau_grid',compact('niveau'));
    }
}
