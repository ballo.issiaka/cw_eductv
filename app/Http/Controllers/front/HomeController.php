<?php

namespace App\Http\Controllers\front;

use App\Classe;
use App\Cours;
use App\Http\Controllers\Controller;
use App\Newsletter;
use App\Niveau;
use App\Notifications\NewCurses;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class HomeController extends Controller
{
    // Index
    public function index(){
        $niveaux  = Niveau::all();
        $lastCurses = Cours::orderBy('id', 'DESC')->limit(5)->get();
        $dossiers = Classe::where('is_moment', 1)->get();
        $newsletter = Newsletter::first();
        //Notification::route('mail', $newsletter->email)->notify(new NewCurses());
        //dd($dossiers);
        $revision = Cours::where('categorie', 'revision')->orderBy('id', 'DESC')->limit(4)->get();
        $contribution = Cours::where('categorie', 'contribution')->get();
        $contributions = array();
        if(count($contribution) > 0){
            foreach ($contribution as $one){
                $contributions[$one->matiere->label]["name"] = $one->matiere->label;
                $contributions[$one->matiere->label]["id"] = $one->matiere->id;
                $contributions[$one->matiere->label]["cours"] = count($one->matiere->cours);
            }
        }
        return view('front/index',compact('niveaux','lastCurses','dossiers','revision','contributions'));
    }

    //


}
