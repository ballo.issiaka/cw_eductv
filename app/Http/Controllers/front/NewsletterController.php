<?php

namespace App\Http\Controllers\front;

use App\Activity;
use App\Http\Controllers\Controller;
use App\Newsletter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class NewsletterController extends Controller
{
    //
    public function store(Request $request){
        $exist = Newsletter::where("email", $request->email_newsletter)->first();
        if($exist){
           $msg = "Déjà enregistré vous recevrez des emails à l'ajout de nouveau cours";
        }
        else{
            try{

                $newsletter = new Newsletter();
                $newsletter->email = $request->email_newsletter;
                $newsletter->slug = uniqid();
                $newsletter->save();
                $msg = "Vous êtes bien inscrit à notre newsletter";
            }catch (\Exception $e){
                dd('une erreur c est produite : '.$e);
            }
        }
        print($msg);

    }
}
