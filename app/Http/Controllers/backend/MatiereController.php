<?php

namespace App\Http\Controllers\backend;

use App\Activity;
use App\Classe;
use App\Http\Controllers\Controller;
use App\Matiere;
use Illuminate\Http\Request;

class MatiereController extends Controller
{
    //
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id){
        $classe = Classe::find($id);

        $matieres = $classe->matieres;
        return view('back.matiere.index', compact('matieres','classe'));
    }

    public function create()
    {
        return view('back.classe.add');
    }

    public function delete($id){
        Matiere::find($id)->delete();
        return redirect()->back();
    }

    public function store(Request $request){
        try{

            $matiere = new Matiere();
            $matiere->label = $request->label;
            $matiere->slug = uniqid();
            $matiere->classe_id = $request->classe;
            $matiere->save();

            // traçabilité des actions

            $activity = new Activity();
            $activity->label = "enregistrement de classe";
            $activity->slug = uniqid();
            $activity->username = 'no-user';
            $activity->user_id = 0;
            $activity->actions = 'l\'utilisateur no_user à enregistré la matiere <b>'.$matiere->slug.'</b>';
            $activity->save();
        }catch (\Exception $e){
            dd('une erreur c est produite : '.$e);
        }

        return redirect()->back();
    }
}
