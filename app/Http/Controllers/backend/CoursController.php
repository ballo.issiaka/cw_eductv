<?php

namespace App\Http\Controllers\backend;

use App\Activity;
use App\Cours;
use App\Http\Controllers\Controller;
use App\Matiere;
use Illuminate\Http\Request;

class CoursController extends Controller
{
    //
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id){
        $matiere = Matiere::find($id);
        $cours = $matiere->cours;
        return view('back.cours.index',compact('cours','matiere'));
    }
    public function create(){
        return view('back.cours.add');
    }

    public function delete($id){
        Cours::find($id)->delete();
        return redirect()->back();
    }

    public function store(Request $request){
        try{
            $cours = new Cours();
            $cours->label = $request->label;
            $cours->description = $request->description;
            $cours->resume = $request->resume;
            $cours->categorie = $request->categorie;
            $cours->video_url = $request->video_url;
            $cours->audio_url = $request->audio_url;
            $cours->professeur = $request->professeur;
            $cours->duree = $request->duree;
            $cours->slug = uniqid();
            $cours->matiere_id = $request->matiere;
            $cours->save();

            // traçabilité des actions

            $activity = new Activity();
            $activity->label = "enregistrement de classe";
            $activity->slug = uniqid();
            $activity->username = 'no-user';
            $activity->user_id = 0;
            $activity->actions = 'l\'utilisateur no_user à enregistré le cours <b>'.$cours->slug.'</b>';
            $activity->save();
        }catch (\Exception $e){
            dd('une erreur c est produite : '.$e);
        }

        return redirect()->back();
    }
}
