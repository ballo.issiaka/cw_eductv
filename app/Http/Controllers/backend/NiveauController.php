<?php

namespace App\Http\Controllers\backend;

use App\Activity;
use App\Http\Controllers\Controller;
use App\Niveau;
use Illuminate\Http\Request;

class NiveauController extends Controller
{
    //
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $niveaux = Niveau::all();
//        dd($niveaux);
        return view('back.niveau.index', compact('niveaux'));
    }

    public function delete($id){
        Niveau::find($id)->delete();
        return redirect()->back();
    }

    public function store(Request $request){
        try{
            $niveau = new Niveau();
            $niveau->label = $request->label;
            $niveau->slug = uniqid();
            $niveau->save();

            // traçabilité des actions

            $activity = new Activity();
            $activity->label = "enregistrement de classe";
            $activity->slug = uniqid();
            $activity->username = 'no-user';
            $activity->user_id = 0;
            $activity->actions = 'l\'utilisateur no_user à enregistré la classe <b>'.$niveau->slug.'</b>';
            $activity->save();
        }catch (\Exception $e){
            dd('une erreur c est produite : '.$e);
        }

        return redirect()->back();
    }
}
