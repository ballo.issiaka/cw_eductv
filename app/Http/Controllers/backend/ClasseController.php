<?php

namespace App\Http\Controllers\backend;

use App\Activity;
use App\Classe;
use App\Http\Controllers\Controller;
use App\Niveau;
use Illuminate\Http\Request;

class ClasseController extends Controller
{
    //
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $classes = Classe::all();
        $niveaux = Niveau::all();
        return view('back.classe.index', compact('classes','niveaux'));
    }

    public function create()
    {
        return view('back.classe.add');
    }

    public function delete($id){
        Classe::find($id)->delete();
        return redirect()->back();
    }

    public function store(Request $request){
        try{
            $classe = new Classe();
            $classe->label = $request->label;
            $classe->slug = uniqid();
            $classe->niveau_id = $request->niveau;
            $classe->is_moment = $request->is_moment;
            $classe->save();

            // traçabilité des actions

            $activity = new Activity();
            $activity->label = "enregistrement de classe";
            $activity->slug = uniqid();
            $activity->username = 'no-user';
            $activity->user_id = 0;
            $activity->actions = 'l\'utilisateur no_user à enregistré la classe <b>'.$classe->slug.'</b>';
            $activity->save();
        }catch (\Exception $e){
            dd('une erreur c est produite : '.$e);
        }

        return redirect()->back();
    }
}
