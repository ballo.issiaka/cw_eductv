<?php

namespace App\Http\Controllers;

use App\Classe;
use App\Cours;
use App\Http\Controllers\Controller;
use App\Niveau;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $niveaux  = Niveau::all();
        $lastCurses = Cours::orderBy('id', 'DESC')->limit(5)->get();
        $dossiers = Classe::where('is_moment', 1)->get();
        //dd($dossiers);
        return view('front/index',compact('niveaux','lastCurses','dossiers'));
    }
}
