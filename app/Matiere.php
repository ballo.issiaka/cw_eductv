<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Matiere extends Model
{
    //
    protected $fillable = ['label', 'slug'];

    public function classe(){
        return $this->belongsTo(Classe::class);
    }
    public function cours(){
        return $this->hasMany(Cours::class);
    }
}
