<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Front Routes
|--------------------------------------------------------------------------
*/

Route::get('/niveau/classe/{id}', "front\ClasseController@index")->name('front.matieres.list');
Route::get('/niveau/{id}', "front\NiveauController@index")->name('front.niveaux.list');
Route::get('/niveau/classe/{matiere}/{id}', "front\MatiereController@index")->name('front.cours-detail');
Route::get('/', "front\HomeController@index")->name('home');

// *****************  Cours audio routes *****************************

Route::get('/niveau/cours-audio',"backend\CoursController@audio")->name('cours.audio.niveau');


// *************************************************************************
// *************************************************************************

// ********************  Cours Routes ********************************

Route::get('backend/listes',"backend\CoursController@index")->name('cours.list');
Route::get('backend/ajout',"backend\CoursController@create")->name('cours.create');

// ********************  Classe Routes ********************************

Route::get('backend/classes',"backend\ClasseController@index")->name('classes.index');
Route::post('backend/classes/add',"backend\ClasseController@store")->name('classes.store');
Route::get('backend/classes/delete/{id}',"backend\ClasseController@delete")->name('classes.delete');

// ********************  Matiere Routes ********************************

Route::get('backend/matieres/{id}',"backend\MatiereController@index")->name('matieres.index');
Route::post('backend/matiere/add',"backend\MatiereController@store")->name('matieres.store');
Route::get('backend/matiere/delete/{id}',"backend\MatiereController@delete")->name('matieres.delete');

// ********************  Cours Routes ********************************

Route::get('backend/cours/{id}',"backend\CoursController@index")->name('cours.index');
Route::post('backend/cours/add',"backend\CoursController@store")->name('cours.store');
Route::get('backend/cours/delete/{id}',"backend\CoursController@delete")->name('cours.delete');

// ********************  Niveau Routes ********************************

Route::get('backend/',"backend\NiveauController@index")->name('backend');
Route::get('backend/niveaux',"backend\NiveauController@index")->name('niveaux.index');
Route::post('backend/niveaux/add',"backend\NiveauController@store")->name('niveaux.store');
Route::get('backend/niveaux/delete/{id}',"backend\NiveauController@delete")->name('niveaux.delete');

// *****************  Professeurs routes *****************************

Route::get('backend/prof/listes',"backend\ProfesseursController@index")->name('prof.list');
Route::get('backend/prof/ajout',"backend\ProfesseursController@create")->name('prof.create');


Route::get('/home', 'HomeController@index')->name('home');

// *****************  Authentication routes *****************************

Auth::routes();
Route::match(['get', 'post'], "/logout", "Auth\LoginController@logout")->name('logout');
Route::get("backend/register", "Auth\RegisterController@showRegistrationForm")->name('register.form');
Route::post("/register", "Auth\RegisterController@register")->name('register');


//newsletter registry
Route::post("/newsletter", "front\NewsletterController@store")->name('newsletter.registry');
