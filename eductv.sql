-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 15, 2020 at 06:56 PM
-- Server version: 5.7.14
-- PHP Version: 7.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eductv`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
                              `id` bigint(20) UNSIGNED NOT NULL,
                              `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                              `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                              `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                              `actions` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                              `created_at` timestamp NULL DEFAULT NULL,
                              `updated_at` timestamp NULL DEFAULT NULL,
                              `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `label`, `slug`, `username`, `actions`, `created_at`, `updated_at`, `user_id`) VALUES
(1, 'enregistrement de classe', '5e97533cf3ce6', 'no-user', 'l\'utilisateur no_user à enregistré la classe <b>5e97533cf186f</b>', '2020-04-15 18:32:28', '2020-04-15 18:32:28', 0),
(2, 'enregistrement de classe', '5e9753410719a', 'no-user', 'l\'utilisateur no_user à enregistré la classe <b>5e9753410499f</b>', '2020-04-15 18:32:33', '2020-04-15 18:32:33', 0),
(3, 'enregistrement de classe', '5e9753460d6d0', 'no-user', 'l\'utilisateur no_user à enregistré la classe <b>5e9753460a8f8</b>', '2020-04-15 18:32:38', '2020-04-15 18:32:38', 0),
(4, 'enregistrement de classe', '5e9753462ddef', 'no-user', 'l\'utilisateur no_user à enregistré la classe <b>5e9753462a088</b>', '2020-04-15 18:32:38', '2020-04-15 18:32:38', 0),
(5, 'enregistrement de classe', '5e97534b1c1d7', 'no-user', 'l\'utilisateur no_user à enregistré la classe <b>5e97534b18f1c</b>', '2020-04-15 18:32:43', '2020-04-15 18:32:43', 0),
(6, 'enregistrement de classe', '5e9753505d8e6', 'no-user', 'l\'utilisateur no_user à enregistré la classe <b>5e9753505af85</b>', '2020-04-15 18:32:48', '2020-04-15 18:32:48', 0),
(7, 'enregistrement de classe', '5e975354cf748', 'no-user', 'l\'utilisateur no_user à enregistré la classe <b>5e975354cc28c</b>', '2020-04-15 18:32:52', '2020-04-15 18:32:52', 0),
(8, 'enregistrement de classe', '5e97535a97369', 'no-user', 'l\'utilisateur no_user à enregistré la classe <b>5e97535a935f9</b>', '2020-04-15 18:32:58', '2020-04-15 18:32:58', 0),
(9, 'enregistrement de classe', '5e97535eb8052', 'no-user', 'l\'utilisateur no_user à enregistré la classe <b>5e97535eb4d7f</b>', '2020-04-15 18:33:02', '2020-04-15 18:33:02', 0),
(10, 'enregistrement de classe', '5e9753a49a8de', 'no-user', 'l\'utilisateur no_user à enregistré la classe <b>5e9753a4976e4</b>', '2020-04-15 18:34:12', '2020-04-15 18:34:12', 0),
(11, 'enregistrement de classe', '5e9753b7736e2', 'no-user', 'l\'utilisateur no_user à enregistré la classe <b>5e9753b77044c</b>', '2020-04-15 18:34:31', '2020-04-15 18:34:31', 0),
(12, 'enregistrement de classe', '5e9753c7b4fbb', 'no-user', 'l\'utilisateur no_user à enregistré la classe <b>5e9753c7b2362</b>', '2020-04-15 18:34:47', '2020-04-15 18:34:47', 0),
(13, 'enregistrement de classe', '5e9753d899a0f', 'no-user', 'l\'utilisateur no_user à enregistré la classe <b>5e9753d8973b7</b>', '2020-04-15 18:35:04', '2020-04-15 18:35:04', 0),
(14, 'enregistrement de classe', '5e9753ee4bdd6', 'no-user', 'l\'utilisateur no_user à enregistré la classe <b>5e9753ee497df</b>', '2020-04-15 18:35:26', '2020-04-15 18:35:26', 0),
(15, 'enregistrement de classe', '5e97540b5c854', 'no-user', 'l\'utilisateur no_user à enregistré la classe <b>5e97540b58092</b>', '2020-04-15 18:35:55', '2020-04-15 18:35:55', 0),
(16, 'enregistrement de classe', '5e97586142afb', 'no-user', 'l\'utilisateur no_user à enregistré la matiere <b>5e9758613f9fd</b>', '2020-04-15 18:54:25', '2020-04-15 18:54:25', 0);

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
                           `id` bigint(20) UNSIGNED NOT NULL,
                           `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                           `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                           `niveau_id` int(10) UNSIGNED NOT NULL,
                           `is_moment` int(11) NOT NULL DEFAULT '0',
                           `created_at` timestamp NULL DEFAULT NULL,
                           `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`id`, `label`, `slug`, `niveau_id`, `is_moment`, `created_at`, `updated_at`) VALUES
(1, 'CM2', '5e9753a4976e4', 5, 1, '2020-04-15 18:34:12', '2020-04-15 18:34:12'),
(2, '3ème', '5e9753b77044c', 6, 1, '2020-04-15 18:34:31', '2020-04-15 18:34:31'),
(3, 'terminale A', '5e9753c7b2362', 7, 1, '2020-04-15 18:34:47', '2020-04-15 18:34:47'),
(4, 'terminale D', '5e9753d8973b7', 7, 1, '2020-04-15 18:35:04', '2020-04-15 18:35:04'),
(5, 'terminale C', '5e9753ee497df', 7, 1, '2020-04-15 18:35:26', '2020-04-15 18:35:26'),
(6, 'terminales', '5e97540b58092', 8, 1, '2020-04-15 18:35:55', '2020-04-15 18:35:55');

-- --------------------------------------------------------

--
-- Table structure for table `cours`
--

CREATE TABLE `cours` (
                         `id` bigint(20) UNSIGNED NOT NULL,
                         `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `categorie` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `resume` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `audio_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `video_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `professeur` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `duree` int(11) DEFAULT NULL,
                         `created_at` timestamp NULL DEFAULT NULL,
                         `updated_at` timestamp NULL DEFAULT NULL,
                         `matiere_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
                               `id` bigint(20) UNSIGNED NOT NULL,
                               `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
                               `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
                               `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
                               `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
                               `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `matieres`
--

CREATE TABLE `matieres` (
                            `id` bigint(20) UNSIGNED NOT NULL,
                            `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                            `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                            `created_at` timestamp NULL DEFAULT NULL,
                            `updated_at` timestamp NULL DEFAULT NULL,
                            `classe_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `matieres`
--

INSERT INTO `matieres` (`id`, `label`, `slug`, `created_at`, `updated_at`, `classe_id`) VALUES
(1, 'français', '5e9758613f9fd', '2020-04-15 18:54:25', '2020-04-15 18:54:25', 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
                              `id` int(10) UNSIGNED NOT NULL,
                              `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                              `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2020_04_13_130955_create_classes_table', 1),
(4, '2020_04_13_131317_create_matieres_table', 1),
(5, '2020_04_13_131933_create_cours_table', 1),
(6, '2020_04_13_133109_create_activities_table', 1),
(7, '2020_04_13_144818_create_niveaux_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `niveaux`
--

CREATE TABLE `niveaux` (
                           `id` bigint(20) UNSIGNED NOT NULL,
                           `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                           `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                           `created_at` timestamp NULL DEFAULT NULL,
                           `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `niveaux`
--

INSERT INTO `niveaux` (`id`, `label`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'préscolaire', '5e97533cf186f', '2020-04-15 18:32:28', '2020-04-15 18:32:28'),
(2, 'CP', '5e9753410499f', '2020-04-15 18:32:33', '2020-04-15 18:32:33'),
(3, 'CE', '5e9753460a8f8', '2020-04-15 18:32:38', '2020-04-15 18:32:38'),
(4, 'CE', '5e9753462a088', '2020-04-15 18:32:38', '2020-04-15 18:32:38'),
(5, 'CM', '5e97534b18f1c', '2020-04-15 18:32:43', '2020-04-15 18:32:43'),
(6, 'collège', '5e9753505af85', '2020-04-15 18:32:48', '2020-04-15 18:32:48'),
(7, 'lycée', '5e975354cc28c', '2020-04-15 18:32:52', '2020-04-15 18:32:52'),
(8, 'enseignement technique', '5e97535a935f9', '2020-04-15 18:32:58', '2020-04-15 18:32:58'),
(9, 'enseignement professionnel', '5e97535eb4d7f', '2020-04-15 18:33:02', '2020-04-15 18:33:02');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
                         `id` bigint(20) UNSIGNED NOT NULL,
                         `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                         `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                         `email_verified_at` timestamp NULL DEFAULT NULL,
                         `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                         `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `created_at` timestamp NULL DEFAULT NULL,
                         `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
    ADD PRIMARY KEY (`id`),
    ADD KEY `activities_user_id_foreign` (`user_id`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
    ADD PRIMARY KEY (`id`),
    ADD KEY `classes_niveau_id_foreign` (`niveau_id`);

--
-- Indexes for table `cours`
--
ALTER TABLE `cours`
    ADD PRIMARY KEY (`id`),
    ADD KEY `cours_matiere_id_foreign` (`matiere_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `matieres`
--
ALTER TABLE `matieres`
    ADD PRIMARY KEY (`id`),
    ADD KEY `matieres_classe_id_foreign` (`classe_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `niveaux`
--
ALTER TABLE `niveaux`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
    ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
    MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
    MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `cours`
--
ALTER TABLE `cours`
    MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
    MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `matieres`
--
ALTER TABLE `matieres`
    MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `niveaux`
--
ALTER TABLE `niveaux`
    MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
    MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
