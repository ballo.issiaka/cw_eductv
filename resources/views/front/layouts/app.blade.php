<!DOCTYPE html>
<html lang="fr">

@include('front.common.head')

<body>

    <div id="preloader">
        <div data-loader="circle-side"></div>
    </div>
    <!-- End Preload -->

    @include('front.common.header')
    <!-- /header -->

    @include('front.common.menu')
    <!-- /main_menu -->


        @yield('content')


    <!-- /main -->
    @include('front.common.footer')
    <!--/footer-->

    <!-- Search Menu -->
    @include('front.common.search')
    <!-- / Search Menu -->

    <!-- COMMON SCRIPTS -->
    @include('front.common.scripts')

</body>

</html>
