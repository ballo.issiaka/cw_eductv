<header class="header fadeInDown">
    <div id="logo">
        <a href="{{ route('home') }}">
            <h3><strong style="color: white;">EDUCTV</strong></h3>
        </a>
    </div>
    <ul id="top_menu">
        <li><a href="#0" class="search-overlay-menu-btn">Recherche</a></li>
        <li>
            <div class="hamburger hamburger--spin">
                <div class="hamburger-box">
                    <div class="hamburger-inner"></div>
                </div>
            </div>
        </li>
    </ul>
    <!-- /top_menu -->
</header>
