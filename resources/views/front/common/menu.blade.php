<div id="main_menu">
        <div class="container">
            <nav class="version_2">
                <div class="row">
                    <div class="col-md-3">
                        <h3>PRÉSCOLAIRE</h3>
                        <ul>
                            <li><a href="#">PS</a></li>
                            <li><a href="#">MS</a></li>
                            <li><a href="#">GS</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <h3>PRIMAIRE</h3>
                        <ul>
                            <li><a href="#">CP1</a></li>
                            <li><a href="#">CP2</a></li>
                            <li><a href="#">CE1</a></li>
                            <li><a href="#">CE2</a></li>
                            <li><a href="#">CM1</a></li>
                            <li><a href="#">CM2</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <h3>COLLÈGE</h3>
                        <ul>
                            <li><a href="#">6ème</a></li>
                            <li><a href="#">5ème</a></li>
                            <li><a href="#">4ème</a></li>
                            <li><a href="#">3ème</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <h3>LYCEE</h3>
                        <ul>
                            <li><a href="#">2nd</a></li>
                            <li><a href="#">1ère</a></li>
                            <li><a href="#">Tle</a></li>
                        </ul>
                    </div>
                </div>
                <!-- /row -->
            </nav>
            <div class="follow_us">
                <ul>
                    <li>Suivez nous</li>
                    <li><a href="https://web.facebook.com/EducationTele/" target="_blank"><i class="ti-facebook"></i></a></li>
                    <li><a href="https://www.youtube.com/channel/UCTW4McU91r3mWFqZU9kiSuA?app=desktop" target="_blank"><i class="ti-youtube"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
