<div class="call_section">
    <div class="container clearfix">
        <div class="col-lg-5 col-md-6 float-right wow" data-wow-offset="250">
            <div class="block-reveal">
                <div class="block-vertical"></div>
                <div class="box_1">
                    <h3>Rejoignez nous pour découvrir une nouvelle manière d'apprendre</h3>
                    <p>Nous mettons à votre disposition, des vidéos accéssibles à tout moment et en tous lieux!</p>
                    <a href="#0" class="btn_1 rounded">Voir plus</a>
                </div>
            </div>
        </div>
    </div>
</div>
