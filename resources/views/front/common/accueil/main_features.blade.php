<div class="features clearfix">
    <div class="container">
        <ul>
            <li><i class="pe-7s-study"></i>
                <h4>Vidéos</h4>
            </li>
            <li><i class="pe-7s-target"></i>
                <h4>Idées innovantes</h4>
            </li>
            <li><i class="pe-7s-cup"></i>
                <h4>Activités</h4>
            </li>
        </ul>
    </div>
</div>
