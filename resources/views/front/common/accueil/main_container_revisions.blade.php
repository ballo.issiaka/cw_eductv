<div class="bg_color_1">
    <div class="container margin_120_95">
        <div class="main_title_2">
            <span><em></em></span>
            <h2>Révisions</h2>
        </div>
        <div class="row">
            @if(count($revision) > 0)
                @foreach($revision as $one)
                    <div class="col-lg-6">
                        <a class="box_news" href="{{ route('home') }}">
                            <figure><img src="{{ asset('front/img/IMG_1914.png')}}" alt="">
                                <figcaption><strong>20</strong>Mars</figcaption>
                            </figure>
                            <ul>
                                <li>Révisions {{$one->matiere->classe->label}}</li>
                                <li>{{$one->created_at}}</li>
                            </ul>
                            <h4>{{$one->label}}</h4>
                            <p>{{ucfirst($one->description)}}....</p>
                        </a>
                    </div>
                @endforeach
            @endif
        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
</div>
