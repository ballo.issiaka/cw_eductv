<section class="hero_single">
    <div class="wrapper">
        <div class="container">
            <h3><strong>Côte d’Ivoire</strong><br>École Numerique</h3>
            <p><strong>Ma classe en vidéos</strong></p>
        </div>
        <a href="#first_section" class="btn_explore hidden_tablet"><i class="ti-arrow-down"></i></a>
    </div>
</section>
