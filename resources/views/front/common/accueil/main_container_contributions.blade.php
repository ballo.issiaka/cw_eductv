<div class="container margin_30_95">
            <div class="main_title_2">
                <span><em></em></span>
                <h2>Contribution</h2>
            </div>
            <div class="row">
                @if(count($contributions) > 0)
                    @foreach($contributions as $one)
                        <div class="col-lg-3 col-md-6 wow" data-wow-offset="150">
                            <a href="{{route('front.cours-detail',[$one["id"], "null"])}}" class="grid_item">
                                <figure class="block-reveal">
                                    <div class="block-horizontal"></div>
                                    <img src="{{ asset('front/img/IMG_1914.png')}}" class="img-fluid" alt="">
                                    <div class="info">
                                        <small><i class="ti-layers"></i>{{$one["cours"]}} Vidéos</small>
                                        <h3>{{$one["name"]}}</h3>
                                    </div>
                                </figure>
                            </a>
                        </div>
                    @endforeach
                @else
                    <div class="col-md-12">
                        <div class="alert alert-info">
                            Aucune contribution pour le moment
                        </div>
                    </div>
                @endif
            </div>
            <!-- /row -->
        </div>
