<div class="container margin_30_95">
    <div class="main_title_2">
        <span><em></em></span>
        <h2>Les dossiers du moment</h2>
    </div>
    <div class="row">
        @if(isset($dossiers))
            @foreach($dossiers as $one)
                <div class="col-lg-4 col-md-6 wow" data-wow-offset="150">
                    <a href="{{ route('front.niveaux.list', $one->niveau->id) }}" class="grid_item">
                        <figure class="block-reveal">
                            <div class="block-horizzontal"></div>
                            <img src="{{ asset('./front/img/IMG_1914.png')}}" class="img-fluid" alt="">
                            <div class="info">
                                <small><i class="ti-layers"></i>{{count($one->matieres)}} matières</small>
                                <h3>{{$one->label}}</h3>
                            </div>
                        </figure>
                    </a>
                </div>
            @endforeach
        @endif
        <!-- /grid_item -->

    </div>
    <!-- /row -->
</div>
