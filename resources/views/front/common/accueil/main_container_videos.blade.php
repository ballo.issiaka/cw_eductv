<div class="container-fluid margin_120_0" id="first_section">
    <div class="main_title_2">
        <span><em></em></span>
        <h2>Vidéos à la une</h2>
        <p>{{(!isset($lastCurses) || count($lastCurses) == 0)? 'Aucun cours pour le moment.' : 'Ne manquez pas nos dernières vidéos.'}}</p>
    </div>
    <div id="reccomended" class="owl-carousel owl-theme">
        @if(isset($lastCurses))
            @foreach($lastCurses as $one)

                <div class="item">
                    <div class="box_grid">
                        <figure>
                            <a href="#0" class="wish_bt"></a>
                            <a href="{{ route('front.cours-detail',[$one->matiere->id,$one->id]) }}">
                                <div class="preview"><span>Voir le cours</span></div><img src="{{ asset('front/img/cm2.PNG') }}" class="img-fluid" alt=""></a>
                        </figure>
                        <div class="wrapper">
                            <small>{{$one->matiere->classe->label}}-{{$one->matiere->label}}</small>
                            <h3>{{$one->label}}</h3>
                            <p>{{$one->description}}</p>
                            {{--<div class="rating"><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i><i class="icon_star"></i> <small>(145)</small></div>--}}
                        </div>
                        <ul>
                            <li><i class="icon_clock_alt"></i> {{$one->duree}} min</li>
                                {{--<li><i class="icon_like"></i> 890</li>--}}
                            <li><a href="{{ route('front.cours-detail',[$one->matiere->id, $one->id]) }}">Voir le cours</a></li>
                        </ul>
                    </div>
                </div>
                <!-- /item -->

            @endforeach
        @endif
    </div>
    
    <hr>
</div>
