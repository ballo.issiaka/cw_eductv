<div class="container margin_60_35">
    <div class="main_title_2">
        <span><em></em></span>
        <h2>Retrouver vos vidéos ici</h2>
        <p>Ressources pédagogiques numériques des classes.</p>
    </div>
    <div class="row">
        @if(isset($niveaux))
            <div class="col-lg-4 col-md-6">
            <a class="box_topic" href="{{ route('front.niveaux.list', $niveaux[1]->id) }}">
                <i class="pe-7s-music"></i>
                <h3>Vidéo {{$niveaux[1]->label}}</h3>
            </a>
            </div>
            <div class="col-lg-4 col-md-6">
                <a class="box_topic" href="{{ route('front.niveaux.list', $niveaux[2]->id) }}">
                    <i class="pe-7s-music"></i>
                    <h3>Vidéo {{$niveaux[2]->label}}</h3>
                </a>
            </div>
            <div class="col-lg-4 col-md-6">
                <a class="box_topic" href="{{ route('front.niveaux.list', $niveaux[3]->id) }}">
                    <i class="pe-7s-music"></i>
                    <h3>Vidéo {{$niveaux[3]->label}}</h3>
                </a>
            </div>
            <div class="col-lg-4 col-md-6">
                <a class="box_topic" href="{{ route('front.niveaux.list', $niveaux[0]->id) }}">
                    <i class="pe-7s-music"></i>
                    <h3>Vidéo {{$niveaux[0]->label}}</h3>
                </a>
            </div>
        @endif

        <div class="col-lg-4 col-md-6">
            <a class="box_topic" href="#">
                <i class="pe-7s-music"></i>
                <h3>Cours Audio</h3>
            </a>
        </div>
        <div class="col-lg-4 col-md-6">
            <a class="box_topic" href="#">
                <i class="pe-7s-music"></i>
                <h3>Cours PDF</h3>
            </a>
        </div>
    </div>
    <!--/row-->
</div>
