<!--// Main Banner \\-->
<div class="political-banner">

        <!--// Slider style="margin-bottom: 60px; height: 500px; width: 1000px"\\-->
        <div class="political-banner-layer">
            <img src="{{ asset('extra-images--/banner.jpg') }}" alt="">
            <div class="political-banner-caption">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 right">
                            <div class="political-banner-form">
                                <h1 id="animatedText"></h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--// Slider \\-->
        
    </div>
    <!--// Main Banner \\-->
    @section('add_script')
    
    <script>
        $(function(){
            index= 0;
            text_to_put =   ["Ma nouvelle mission est de reformer l'armée. Faire de l’armée ivoirienne,",
                            "une armée unie, solidaire et républicaine au service de la nation Ivoirienne."];
            $('#animatedText').html(text_to_put[index]);
            setInterval(function(){
                if(index + 1 == text_to_put.length){
                    index = 0;
                }
                else{
                    index++;
                }
                $('#animatedText').fadeOut(500,function(){
                    $('#animatedText').html(text_to_put[index]);
                });
                $('#animatedText').fadeIn();
            }, 5000);
        });
     
    </script>
    
    @endsection
    