<footer>
    <div class="container margin_120_95">
        <div class="row">
            <div class="col-lg-5 col-md-12 p-r-5">
                <p>
                    <h3><strong style="color: white;">EDUCTV</strong></h3>
                </p>
                <p>Nous mettons à votre disposition, des vidéos accéssibles à tout moment et en tous lieux!</p>
                <div class="follow_us">
                    <ul>
                        <li>Suivez nous</li>
                        <li><a href="https://web.facebook.com/EducationTele/" target="_blank"><i class="ti-facebook"></i></a></li>
                        <li><a href="https://www.youtube.com/channel/UCTW4McU91r3mWFqZU9kiSuA?app=desktop" target="_blank"><i class="ti-youtube"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 ml-lg-auto">
                <h5>Liens utiles</h5>
                <ul class="links">
                    <li><a href="#0">PRIMAIRE</a></li>
                    <li><a href="#0">COLLÈGE</a></li>
                    <li><a href="#0">LYCEE</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-6">
                <h5>Conctactez nous</h5>
                <ul>
                    <li>Suivez nous</li>
                    <li><a href="https://web.facebook.com/EducationTele/" target="_blank"><i class="ti-facebook"></i></a></li>
                    <li><a href="https://www.youtube.com/channel/UCTW4McU91r3mWFqZU9kiSuA?app=desktop" target="_blank"><i class="ti-youtube"></i></a></li>
                </ul>
                <div id="newsletter">
                    <h6>Newsletter</h6>
                    <div id="message-newsletter"></div>
                    <form method="post" action="{{route('newsletter.registry')}}" name="newsletter_form" id="newsletter_form">
                        @csrf
                        <div class="form-group">
                            <input type="email" name="email_newsletter" id="email_newsletter" class="form-control" placeholder="Votre email">
                            <input type="submit" value="Envoyer" id="submit-newsletter">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--/row-->
        <hr>
        <div class="row">
            <div class="col-md-8">
                <ul id="additional_links">
                    <li><a href="{{ route('login')}}">Thème et conditions</a></li>
                </ul>
            </div>
            <div class="col-md-4">
                <div id="copy">© 2020 EDUCTV</div>
            </div>
        </div>
    </div>
</footer>
