@extends('front.layouts.app')
@section('content')

<main>
    <div id="error_page">
        <div class="container">
            <div class="row justify-content-center text-center">
                <div class="col-xl-7 col-lg-9">
                    <h2>404 <i class="icon_error-triangle_alt"></i></h2>
                    <p>Nous sommes désolés, mais la page que vous cherchiez n'existe pas.</p>
                    <form>
                        <div class="search_bar_error">
                            <input type="text" class="form-control" placeholder="Que cherchez-vous?">
                            <input type="submit" value="Recherche">
                        </div>
                    </form>
                </div>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /error_page -->

</main>
@endsection
