@extends('front.layouts.app')
@section('content')
    <main>

        @include('front.common.accueil.main_hero_single')
        <!-- /hero_single -->

        @include('front.common.accueil.main_features')
        <!-- /features -->

        @include('front.common.accueil.main_container_ressources')
        <!-- /container -->

        @include('front.common.accueil.main_container_videos')
        <!-- /container -->

        @include('front.common.accueil.main_container_dossiers')
        <!-- /container -->

        @include('front.common.accueil.main_container_revisions')
        <!-- /bg_color_1 -->

        @include('front.common.accueil.main_container_contributions')
        <!-- /container -->

        @include('front.common.accueil.main_container_call_section')
        <!--/call_section-->

    </main>

@endsection
