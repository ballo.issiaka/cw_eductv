@extends('front.layouts.app')
@section('content')


<main>
        <section id="hero_in" class="courses">
            <div class="wrapper">
                <div class="container">
                    <h1 class="fadeInUp"><span></span>{{$classe->label}}</h1>
                </div>
            </div>
        </section>
        <!--/hero_in-->
        <div class="filters_listing sticky_horizzontal">
            <div class="container">
                <ul class="clearfix">
                    <li>
                        <div class="switch-field">

                        </div>
                    </li>
                    <li>
                        <div class="layout_view">
                            <a href="#0" class="active"><i class="icon-th"></i></a>
                        </div>
                    </li>
                    <li>

                    </li>
                </ul>
            </div>
            <!-- /container -->
        </div>
        <!-- /filters -->

        <div class="container margin_30_95">
            <div class="main_title_2">
                <span><em></em></span>
                <h2>Liste des matières - {{$classe->label}}</h2>
            </div>
            <div class="row">
            @if(count($classe->matieres) > 0)
                @foreach($classe->matieres as $one)
                    <!-- /grid_item -->
                        <div class="col-lg-4 col-md-6 wow" data-wow-offset="150">
                            <a href="{{ route('front.cours-detail',[$one->id, "null"]) }}" class="grid_item">
                                <figure class="block-reveal">
                                    <div class="block-horizzontal"></div>
                                    <img src="{{ asset('front/img/cm2.PNG') }}" class="img-fluid" alt="">
                                    <div class="info">
                                        <small><i class="ti-layers"></i>{{count($one->cours)}} Cours</small>
                                        <h3>{{$one->label}}</h3>
                                    </div>
                                </figure>
                            </a>
                        </div>
                    @endforeach
                @else
                    <div class="col-md-12">
                        <div class="alert alert-info">Aucune classe pour ce niveau pour le moment</div>
                    </div>
                @endif
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->

    </main>
    <!--/main-->

@endsection
