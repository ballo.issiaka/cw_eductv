@extends('front.layouts.app')
@section('content')

<main>
    <section id="hero_in" class="courses">
        <div class="wrapper">
            <div class="container">
                <h1 class="fadeInUp"><span></span>{{$matiere->classe->niveau->label}} - {{$matiere->label}}</h1>
            </div>
        </div>
    </section>
    <!--/hero_in-->

    @if(count($matiere->cours) > 0)
        <div class="bg_color_1">
            <nav class="secondary_nav sticky_horizontal">
                <div class="container">
                    <ul class="clearfix">
                        <li><a href="#description" class="active">Description</a></li>
                        <li><a href="#lessons">Lessons</a></li>
                    </ul>
                </div>
            </nav>
            <div class="container margin_60_35">
                <div class="row">

                    <aside class="col-lg-12" id="sidebar">
                        <div class="box_detail">
                            <figure>
                                <a href="{{$cours->video_url}}" class="video"><i class="arrow_triangle-right"></i><img src="{{ asset('front/img/cm2.PNG') }}" alt="" class="img-fluid"><span></span></a>
                            </figure>
                            <div id="list_feat">
                                <h3>Description</h3>
                                <ul>
                                    <li><i class="icon_document_alt"></i>{{$cours->description}}</li>
                                </ul>
                            </div>
                        </div>
                    </aside>


                </div>
                <!-- /row -->
            </div>
            <div class="container margin_60_35">
                <div class="row">

                    <div class="col-lg-12">

                        <section id="description">
                            <h2>Resumé</h2>
                            <p>{{$cours->resume}}</p>

                        </section>
                        <!-- /section -->

                        <section id="lessons">
                            <div class="intro_title">
                                <h2>Cours</h2>
                                <ul>
                                    <li>{{count($matiere->cours)}} cours</li>
                                    <li>{{$durations}} MIN</li>
                                </ul>
                            </div>
                            <div id="accordion_lessons" role="tablist" class="add_bottom_45">
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingOne">
                                        <h5 class="mb-0">
                                            <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="indicator ti-minus"></i>{{$matiere->classe->label}} - {{$matiere->label}}</a>
                                        </h5>
                                    </div>

                                    <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion_lessons">
                                        <div class="card-body">
                                            <div class="list_lessons">
                                                <ul>
                                                    @foreach($matiere->cours as $one)
                                                        <li><a href="{{$one->video_url}}" class="video">{{strtoupper($one->label)}}</a><span>{{$one->duree}} min</span></li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- /accordion -->
                        </section>
                        <!-- /section -->

                    </div>
                    <!-- /col -->

                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
    @else
        <div class="bg_color_1">
            <nav class="secondary_nav sticky_horizontal">
                <div class="container">
                    <ul class="clearfix">
                        <li><a href="#description" class="active">Description</a></li>
                        <li><a href="#lessons">Lessons</a></li>
                    </ul>
                </div>
            </nav>
            <div class="container margin_60_35">
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-info">
                            Aucun cours pour le moment
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <!-- /bg_color_1 -->
</main>
<!--/main-->

@endsection
