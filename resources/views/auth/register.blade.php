<!DOCTYPE html>
<html lang="en">

<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>EDUCTV | Ma classe en vidéo</title>

    <!-- Favicons-->
    <link rel="shortcut icon" href="{{ asset('front/img/favicon.ico')}}" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="{{ asset('front/img/apple-touch-icon-57x57-precomposed.png')}}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="{{ asset('front/img/apple-touch-icon-72x72-precomposed.png')}}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="{{ asset('front/img/apple-touch-icon-114x114-precomposed.png')}}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="{{ asset('front/img/apple-touch-icon-144x144-precomposed.png')}}">

    <!-- BASE CSS -->
    <link href="{{ asset('front/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('front/css/style.css')}}" rel="stylesheet">
    <link href="{{ asset('front/css/vendors.css')}}" rel="stylesheet">
    <link href="{{ asset('front/css/icon_fonts/css/all_icons.min.css')}}" rel="stylesheet">

    <!-- YOUR CUSTOM CSS -->
    <link href="{{ asset('front/css/custom.css')}}" rel="stylesheet">

</head>

<body id="register_bg">

	<div id="preloader">
		<div data-loader="circle-side"></div>
	</div>
	<!-- End Preload -->

	<div id="login">
		<aside>
			<figure>
                <a href="{{ route('home') }}">
                    <h3><strong style="color: white;">EDUCTV</strong></h3>
                </a>
			</figure>
			<form method="POST" action="{{ route('register') }}">
                @csrf
				<div class="form-group">

					<span class="input">
					<input id="name" class="input_field @error('name') is-invalid @enderror" type="text" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
						<label class="input_label">
						<span class="input__label-content">Entrer votre nom</span>
                    </label>
                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
					</span>

					<span class="input">
					<input id="email" class="input_field @error('email') is-invalid @enderror" name="email" type="email" value="{{ old('email') }}" required autocomplete="email">
						<label class="input_label">
						<span class="input__label-content">Entrer votre email</span>
                    </label>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
					</span>

					<span class="input">
					<input class="input_field @error('password') is-invalid @enderror" type="password" id="password" name="password" required autocomplete="new-password">
						<label class="input_label">
						<span class="input__label-content">Entrez votre mot de passe</span>
                    </label>
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
					</span>

					<span class="input">
					<input class="input_field " type="password" id="password-confirm"  name="password_confirmation" required autocomplete="new-password">
						<label class="input_label">
						<span class="input__label-content">Confirmez le mot de passe</span>
					</label>
					</span>

					<div id="pass-info" class="clearfix"></div>
				</div>
                <button type="submit" class="btn_1 rounded full-width add_top_30">
                    {{ __('Inscription') }}
                </button>
				<div class="text-center add_top_10">Vous avez déjà un compte? <strong><a href="{{ route('login') }}">Connexion</a></strong></div>
			</form>
			<div class="copy">© 2020 Educ TV</div>
		</aside>
	</div>
	<!-- /login -->

    <script src="{{ asset('front/js//jquery-2.2.4.min.js')}}"></script>
    <script src="{{ asset('front/js//common_scripts.js')}}"></script>
    <script src="{{ asset('front/js//main.js')}}"></script>
    <script src="{{ asset('front/assets/validate.js')}}"></script>

    <!-- SPECIFIC SCRIPTS -->
    <script src="{{ asset('js/pw_strenght.js')}}"></script>

</body>
</html>
