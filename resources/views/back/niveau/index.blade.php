@extends('back.layouts.app')

@section('title')
    Niveau d'études
@endsection

@section('content')
    <div>
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="/">Tableau de bord</a>
            </li>
            <li class="breadcrumb-item active">niveau</li>
        </ol>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="box_general">
                    <div class="header_box">
                        <h2 class="d-inline-block">Nouvelle enregistrement</h2>
                    </div>
                    <div class="list_general">
                        <form action="{{route('niveaux.store')}}" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Label du niveau</label>
                                        <input type="text" class="form-control" placeholder="Label du niveau" name="label">
                                    </div>
                                </div>
                            </div>
                            <div class="row pb-3">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-outline-success form-control">Valider</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="box_general">
            <div class="header_box">
                <h2 class="d-inline-block">Les Niveaux enregistrées</h2>
            </div>
            <div class="list_general">
                <div class="row pb-3">
                    @if(isset($niveaux))
                        @foreach($niveaux as $niveau)
                            <div class="col-md-3 ">
                                <div class="card">
                                    <div class="card-header">
                                        <h4><i class="text text-warning">Niveau</i> :: {{$niveau->label}} </h4><sub class="badge badge-dark">SLUG: {{$niveau->slug}}</sub>
                                    </div>
                                    <div class="card-body">
                                        <ul>
                                            <li>Ajouté le: {{$niveau->created_at}}</li>
                                        </ul>
                                    </div>
                                    <div class="card-footer">
                                        <form action="{{route('niveaux.delete', $niveau->id)}}">
                                            @csrf
                                            <button class="btn btn-outline-danger" type="submit"> <i class="fa fa-trash-o"></i></button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
        <!-- /box_general-->
        <nav aria-label="...">
            <ul class="pagination pagination-sm add_bottom_30">
                <li class="page-item disabled">
                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                </li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                    <a class="page-link" href="#">Next</a>
                </li>
            </ul>
        </nav>
        <!-- /pagination-->
    </div>
@endsection
