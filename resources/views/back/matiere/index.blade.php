@extends('back.layouts.app')

@section('title')
    Liste des Matiere de la classe {{$classe->label}}
@endsection

@section('content')
    <div>
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="/">Tableau de bord</a>
            </li>
            <li class="breadcrumb-item active">{{$classe->slug}} <i class="badge badge-black">{{$classe->label}}</i></li>
        </ol>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="box_general">
                    <div class="header_box">
                        <h2 class="d-inline-block">Nouvelle enregistrement :: <b>{{$classe->label}}</b></h2>
                    </div>
                    <div class="list_general">
                        <form action="{{route('matieres.store')}}" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Label matière :</label>
                                        <input type="text" class="form-control" placeholder="Label de la matière" name="label">
                                    </div>
                                    <input type="hidden" value="{{$classe->id}}" name="classe">
                                </div>
                            </div>

                            <div class="row pb-3">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-outline-success form-control">Valider</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="box_general">
            <div class="header_box">
                <h2 class="d-inline-block">Les matières enregistrées</h2>
            </div>
            <div class="list_general">
                <div class="row pb-3">
                    @if(isset($matieres))
                        @foreach($matieres as $matiere)
                            <div class="col-md-3 pb-3">
                                <div class="card">
                                    <div class="card-header">
                                        <h5><i class="text text-warning">{{$classe->label}}</i> :: {{$matiere->label}} </h5><sub class="badge badge-dark">SLUG: {{$matiere->slug}}</sub>
                                    </div>
                                    <div class="card-body">
                                        <ul>
                                            <li>Ajouté le: {{$matiere->created_at}}</li>
                                        </ul>
                                    </div>
                                    <div class="card-footer">
                                        <div class="row pr-2">
                                            <div class="col-md-6">
                                                <form action="{{route('matieres.delete', $matiere->id)}}">
                                                    @csrf
                                                    <button class="btn btn-outline-danger" type="submit"> <i class="fa fa-trash-o"></i></button>
                                                </form>
                                            </div>
                                            <div class="col-md-6 ">
                                                <a href="{{route('cours.index', $matiere->id)}}">
                                                    <button class="btn btn-outline-success"> Les Cours</button>
                                                </a>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
        <!-- /box_general-->
        <nav aria-label="...">
            <ul class="pagination pagination-sm add_bottom_30">
                <li class="page-item disabled">
                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                </li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                    <a class="page-link" href="#">Next</a>
                </li>
            </ul>
        </nav>
        <!-- /pagination-->
    </div>
@endsection
