@extends('back.layouts.app')

@section('title')
    Liste des profs
@endsection

@section('content')
    <div>
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="/">Tableau de bord</a>
            </li>
            <li class="breadcrumb-item active">Vos cours</li>
        </ol>
        <div class="box_general">
            <div class="header_box">
                <h2 class="d-inline-block">Your Courses</h2>
                <div class="filter">
                    <select name="orderby" class="selectbox">
                        <option value="Any status">Any status</option>
                        <option value="Approved">Started</option>
                        <option value="Pending">Pending</option>
                        <option value="Cancelled">Cancelled</option>
                    </select>
                </div>
            </div>
            <div class="list_general">
                <ul>
                    <li>
                        <h4>Course title <i class="approved">validated</i></h4>
                        <ul class="buttons">
                            <li><a href="#0" class="btn_1 gray approve"><i class="fa fa-fw fa-check-circle-o"></i> Approve</a></li>
                            <li><a href="#0" class="btn_1 gray delete"><i class="fa fa-fw fa-times-circle-o"></i> Cancel</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /box_general-->
        <nav aria-label="...">
            <ul class="pagination pagination-sm add_bottom_30">
                <li class="page-item disabled">
                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                </li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                    <a class="page-link" href="#">Next</a>
                </li>
            </ul>
        </nav>
        <!-- /pagination-->
    </div>
@endsection
