@extends('back.layouts.app')

@section('title')
    Liste des cours de la matière {{$matiere->label}}
@endsection

@section('content')
    <div>
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="/">Tableau de bord</a>
            </li>
            <li class="breadcrumb-item active">{{$matiere->slug}} <i class="badge badge-black">{{$matiere->label}}</i></li>
        </ol>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="box_general">
                    <div class="header_box">
                        <h2 class="d-inline-block">Nouvel enregistrement :: <b>{{$matiere->label}}</b> :: <b class="text text-danger">{{$matiere->classe->label}}</b></h2>
                    </div>
                    <div class="list_general">
                        <form action="{{route('cours.store')}}" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Label cours :</label>
                                        <input type="text" class="form-control" placeholder="Label de la matière" name="label">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>catégorie cours :</label>
                                        <select  class="form-control"  name="categorie">
                                            <option value="general">General</option>
                                            <option value="revision">Révision</option>
                                            <option value="contribution">Contribution</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>description cours :</label>
                                        <textarea class="form-control" placeholder="Label de la matière" name="description">
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>lien vidéo :</label>
                                        <input type="text" class="form-control" placeholder="Lien youtube de la video" name="video_url">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>lien audio :</label>
                                        <input type="text" class="form-control" placeholder="lien de l'audio" name="audio_url">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Resumé cours :</label>
                                        <textarea class="form-control" placeholder="Label de la matière" name="resume">
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label>Nom et prénom du professeur dispensant le cours :</label>
                                        <input type="text" class="form-control" placeholder="Nom & Prénom" name="professeur">
                                    </div>
                                    <input type="hidden" value="{{$matiere->id}}" name="matiere">
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>-Durée du cours:</label>
                                        <input type="text" class="form-control" placeholder="Durée du cours en minutes" name="duree">
                                    </div>
                                </div>
                            </div>

                            <div class="row pb-3">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-outline-success form-control">Valider</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="box_general">
            <div class="header_box">
                <h2 class="d-inline-block">Les cours enregistrés</h2>
            </div>
            <div class="list_general">
                <div class="row pb-3">
                    @if(isset($cours))
                        @foreach($cours as $one)
                            <div class="col-md-3 pb-3">
                                <div class="card">
                                    <div class="card-header">
                                        <h5><i class="text text-warning">{{$matiere->label}}</i> :: {{$one->label}} </h5><sub class="badge badge-dark">SLUG: {{$one->slug}}</sub>
                                    </div>
                                    <div class="card-body">
                                        <ul>
                                            <li>Voir la video: <a target="_blank" href="{{$one->video_url}}">lien -- <i class="fa fa-video-camera"></i></a></li>
                                            <li>ecouter l'audio: <a target="_blank" href="{{$one->audio_url}}">lien -- <i class="fa fa-file-audio-o"></i></a></li>
                                            <li>Ajouté le: {{$one->created_at}}</li>
                                        </ul>
                                        <hr>
                                        <h5>Description</h5>
                                        <p>
                                            {{$one->description}}
                                        </p>
                                    </div>
                                    <div class="card-footer">
                                        <div class="row pr-2">
                                            <div class="col-md-6">
                                                <form action="{{route('cours.delete', $one->id)}}">
                                                    @csrf
                                                    <button class="btn btn-outline-danger" type="submit"> <i class="fa fa-trash-o"></i></button>
                                                </form>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
        <!-- /box_general-->
        <nav aria-label="...">
            <ul class="pagination pagination-sm add_bottom_30">
                <li class="page-item disabled">
                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                </li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                    <a class="page-link" href="#">Next</a>
                </li>
            </ul>
        </nav>
        <!-- /pagination-->
    </div>
@endsection

