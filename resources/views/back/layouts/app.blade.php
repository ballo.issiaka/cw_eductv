<!DOCTYPE html>
<html lang="fr">

@include('back.common.head')

<body class="fixed-nav sticky-footer" id="page-top">

<!-- Begin page -->
<div id="app">

    @include('back.common.navbar')
    <div class="content-wrapper">
        <div class="container-fluid">
            <!-- Start content -->
            @yield('content')
            <!-- content -->
        </div>
    </div>
    @include('back.common.footer')
</div>
@include('back.common.scripts')
</body>

</html>