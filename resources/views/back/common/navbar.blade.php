<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-dark bg-default fixed-top" id="mainNav">
        <a href="{{ route('home') }}">
            <h3><strong style="color: white;">EDUCTV</strong></h3>
        </a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
                <a class="nav-link" href="index.html">
                    <i class="fa fa-fw fa-dashboard"></i>
                    <span class="nav-link-text">Tableau de bord</span>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Niveaux">
                <a class="nav-link" href="{{route('niveaux.index')}}">
                    <i class="fa fa-fw fa-university"></i>
                    <span class="nav-link-text">Niveaux</span>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Reviews">
                <a class="nav-link" href="{{route('classes.index')}}">
                    <i class="fa fa-fw fa-home"></i>
                    <span class="nav-link-text">Classes</span>
                </a>
            </li>
            {{--<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Bookings">
                <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseCours" data-parent="#exampleAccordion">
                    <i class="fa fa-fw fa-archive"></i>
                    <span class="nav-link-text">Cours</span><span class="badge badge-pill badge-primary">6 Nouveaux</span>
                </a>
                <ul class="sidenav-second-level collapse" id="collapseCours">
                    <li>

                        <a href="/listes">
                            <i class="fa fa-fw fa-list"></i>
                            <span class="nav-link-text">Listes</span>
                        </a>
                    </li>
                    <li>
                        <a href="/ajout">
                            <i class="fa fa-fw fa-plus-square"></i>
                            <span class="nav-link-text">Ajouter</span>
                        </a>
                    </li>
                </ul>
            </li>--}}
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Reviews">
                <a class="nav-link" href="#">
                    <i class="fa fa-fw fa-cogs"></i>
                    <span class="nav-link-text">Paramètres</span>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav sidenav-toggler">
            <li class="nav-item">
                <a class="nav-link text-center" id="sidenavToggler">
                    <i class="fa fa-fw fa-angle-left"></i>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle mr-lg-2" id="messagesDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-fw fa-envelope"></i>
                    <span class="d-lg-none">Messages
              <span class="badge badge-pill badge-primary">12 New</span>
            </span>
                    <span class="indicator text-primary d-none d-lg-block">
              <i class="fa fa-fw fa-circle"></i>
            </span>
                </a>
                <div class="dropdown-menu" aria-labelledby="messagesDropdown">
                    <h6 class="dropdown-header">New Messages:</h6>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">
                        <strong>John Doe</strong>
                        <span class="small float-right text-muted">11:21 AM</span>
                        <div class="dropdown-message small">I've sent the final files over to you for review. When you're able to sign off of them let me know and we can discuss distribution.</div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item small" href="#">View all messages</a>
                </div>
            </li>

            <li class="nav-item">
                <form class="form-inline my-2 my-lg-0 mr-lg-2">
                    <div class="input-group">
                        <input class="form-control search-top" type="text" placeholder="Recherche...">
                        <span class="input-group-btn">
                            <button class="btn btn-primary" type="button">
                            <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                </form>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
                    <i class="fa fa-fw fa-sign-out"></i>Quitter</a>
            </li>
        </ul>
    </div>
</nav>
<!-- /Navigation-->
