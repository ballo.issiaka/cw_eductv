@extends('back.layouts.app')

@section('title')
    Liste des Classes
@endsection

@section('content')
    <div>
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="/">Tableau de bord</a>
            </li>
            <li class="breadcrumb-item active">Vos cours</li>
        </ol>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="box_general">
                    <div class="header_box">
                        <h2 class="d-inline-block">Nouvelle enregistrement</h2>
                    </div>
                    <div class="list_general">
                        <form action="{{route('classes.store')}}" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Label de la classe : </label>
                                        <input type="text" class="form-control" placeholder="Label de la classe" name="label">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for ="is_moment">dossier du moment ? </label>
                                        <select type="checkbox" class="form-control" name="is_moment" id="is_moment">
                                            <option value="0">non</option>
                                            <option value="1">oui</option>
                                        </select>
                                    </div>
                                </div>
                            </div><div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Niveau d'étude</label>
                                        <select  class="form-control" placeholder="Label de la classe" name="niveau">
                                            @if(isset($niveaux))
                                                @foreach($niveaux as $niveau)
                                                    <option value="{{$niveau->id}}">{{$niveau->label}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row pb-3">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-outline-success form-control">Valider</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="box_general">
            <div class="header_box">
                <h2 class="d-inline-block">Les classes enregistrées</h2>
            </div>
            <div class="list_general">
                <div class="row pb-3">
                    @if(isset($classes))
                        @foreach($classes as $classe)
                            <div class="col-md-3 pb-3">
                                <div class="card">
                                    <div class="card-header">
                                        <h5><i class="text text-warning">Niveau</i> :: {{$classe->niveau->label}}
                                            <br> <i class="text text-warning">Classe</i> :: {{$classe->label}} </h5><sub class="badge badge-dark">SLUG: {{$classe->slug}}</sub>
                                    </div>
                                    <div class="card-body">
                                        <ul>
                                            <li>Ajouté le: {{$classe->created_at}}</li>
                                        </ul>
                                    </div>
                                    <div class="card-footer">
                                        <div class="row pr-5">
                                            <div class="col-md-6">
                                                <form action="{{route('classes.delete', $classe->id)}}">
                                                    @csrf
                                                    <button class="btn btn-outline-danger" type="submit"> <i class="fa fa-trash-o"></i></button>
                                                </form>
                                            </div>
                                            <div class="col-md-6 ">
                                                <a href="{{route('matieres.index',$classe->id)}}">
                                                    <button class="btn btn-outline-success"> Les Matieres</button>
                                                </a>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
        <!-- /box_general-->
        <nav aria-label="...">
            <ul class="pagination pagination-sm add_bottom_30">
                <li class="page-item disabled">
                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                </li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                    <a class="page-link" href="#">Next</a>
                </li>
            </ul>
        </nav>
        <!-- /pagination-->
    </div>
@endsection
